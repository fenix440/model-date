<?php  namespace Fenix440\Model\Date\Exceptions; 

/**
 * Class InvalidDateException
 *
 * Throws an exception if date is invalid
 *
 * @package Fenix440\Model\Date\Exceptions 
 * @author      Bartlomiej Szala <fenix440@gmail.com>
*/
class InvalidDateException extends \InvalidArgumentException{

 

}

 