<?php namespace Fenix440\Model\Date\Interfaces;
use Fenix440\Model\Date\Exceptions\InvalidDateException;

/**
 * Interface DateAware
 *
 * A component/object must be aware of date
 *
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 * @package      Fenix440\Model\Date\Interfaces
*/
interface DateAware {

    /**
     * Set date for given component
     *
     * @param \DateTime $date   Date for given component
     * @return void
     * @throws InvalidDateException If date is invalid
     */
    public function setDate($date);

    /**
     * Validates date if is valid
     * @param mixed $date   Date for given component
     * @return bool true/false
     */
    public function isDateValid($date);

    /**
     * Get date
     * @return \DateTime|null
     */
    public function getDate();

    /**
     * Get default date
     *
     * @return \DateTime|null
     */
    public function getDefaultDate();

    /**
     * Checks if default date is set
     *
     * @return bool true/false
     */
    public function hasDefaultDate();

    /**
     * Checks if date is set
     *
     * @return bool true/false
     */
    public function hasDate();

}