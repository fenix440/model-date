<?php namespace Fenix440\Model\Date\Interfaces;
use Fenix440\Model\Date\Exceptions\InvalidDateException;

/**
 * Interface EndDateAware
 *
 * A component must be aware of End date property
 *
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 * @package      Fenix440\Model\Date\Interfaces
*/
interface EndDateAware {

    /**
     * Set End date for given component
     * @param \DateTime $endDate End date for given component
     * @return void
     * @throws InvalidDateException If End date is invalid
     */
    public function setEndDate($endDate);

    /**
     * Validates if End date is valid
     * @param mixed $endDate Enddate for given component
     * @return bool true/false
     */
    public function isEndDateValid($endDate);

    /**
     * Get End date
     *
     * @return \DateTime|null
     */
    public function getEndDate();

    /**
     * Get default End date
     *
     * @return \DateTime|null
     */
    public function getDefaultEndDate();

    /**
     * Checks if default End date is set
     *
     * @return bool true/false
     */
    public function hasDefaultEndDate();

    /**
     * Checks if End date is set
     *
     * @return bool true/false
     */
    public function hasEndDate();

}