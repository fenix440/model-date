<?php namespace Fenix440\Model\Date\Interfaces;
use Fenix440\Model\Date\Exceptions\InvalidDateException;

/**
 * Interface StartDateAware
 *
 * A component must be aware of start date property
 *
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 * @package      Fenix440\Model\Date\Interfaces
*/
interface StartDateAware {

    /**
     * Set start date for given component
     * @param \DateTime $startDate Start date for given component
     * @return void
     * @throws InvalidDateException If start date is invalid
     */
    public function setStartDate($startDate);

    /**
     * Validates if start date is valid
     * @param mixed $startDate Start date for given component
     * @return bool true/false
     */
    public function isStartDateValid($startDate);

    /**
     * Get start date
     *
     * @return \DateTime|null
     */
    public function getStartDate();

    /**
     * Get default start date
     *
     * @return \DateTime|null
     */
    public function getDefaultStartDate();

    /**
     * Checks if default start date is set
     *
     * @return bool true/false
     */
    public function hasDefaultStartDate();

    /**
     * Checks if start date is set
     *
     * @return bool true/false
     */
    public function hasStartDate();

}