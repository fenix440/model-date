<?php  namespace Fenix440\Model\Date\Traits;
use Aedart\Validate\Date\DateValidator;
use Fenix440\Model\Date\Exceptions\InvalidDateException;

/**
 * Trait StartDateTrait
 *
 * @see StartDateAware
 *
 * @package      Fenix440\Model\Date\Traits
 * @author      Bartlomiej Szala <fenix440@gmail.com>
*/
trait StartDateTrait {

    /**
     * Start date for given component
     * @var null|\DateTime
     */
    protected $startDate=null;

    /**
     * Set start date for given component
     * @param \DateTime $startDate Start date for given component
     * @return void
     * @throws InvalidDateException If start date is invalid
     */
    public function setStartDate($startDate){
        if(!$this->isStartDateValid($startDate))
            throw new InvalidDateException(sprintf('Start date %s is invalid',var_export($startDate,true)));
        $this->startDate=$startDate;
    }

    /**
     * Validates if start date is valid
     * @param mixed $startDate Start date for given component
     * @return bool true/false
     */
    public function isStartDateValid($startDate){
        return DateValidator::isValid($startDate);
    }

    /**
     * Get start date
     *
     * @return \DateTime|null
     */
    public function getStartDate(){
        if(!$this->hasStartDate() && $this->hasDefaultStartDate())
            $this->setStartDate($this->getDefaultStartDate());
        return $this->startDate;
    }

    /**
     * Get default start date
     *
     * @return \DateTime|null
     */
    public function getDefaultStartDate(){
        return null;
    }

    /**
     * Checks if default start date is set
     *
     * @return bool true/false
     */
    public function hasDefaultStartDate(){
        return (!is_null($this->getDefaultStartDate()))? true:false;
    }

    /**
     * Checks if start date is set
     *
     * @return bool true/false
     */
    public function hasStartDate(){
        return (!is_null($this->startDate))? true:false;
    }


}