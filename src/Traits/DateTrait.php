<?php  namespace Fenix440\Model\Date\Traits;
use Aedart\Validate\Date\DateValidator;
use Fenix440\Model\Date\Exceptions\InvalidDateException;

/**
 * Trait DateTrait
 *
 * @see DateAware
 *
 * @package      Fenix440\Model\Date\Traits
 * @author      Bartlomiej Szala <fenix440@gmail.com>
*/
trait DateTrait {

    /**
     * Date for given component
     * @var null|\DateTime
     */
    protected $date=null;

    /**
     * Set date for given component
     *
     * @param \DateTime $date   Date for given component
     * @return void
     * @throws InvalidDateException If date is invalid
     */
    public function setDate($date){
        if(!$this->isDateValid($date))
            throw new InvalidDateException(sprintf('Date %s is invalid',$date));
        $this->date=$date;
    }

    /**
     * Validates date if is valid
     * @param mixed $date   Date for given component
     * @return bool true/false
     */
    public function isDateValid($date){
        return DateValidator::isValid($date);
    }

    /**
     * Get date
     * @return \DateTime|null
     */
    public function getDate(){
        if(!$this->hasDate() && $this->hasDefaultDate())
            $this->setDate($this->getDefaultDate());
        return $this->date;
    }

    /**
     * Get default date
     *
     * @return \DateTime|null
     */
    public function getDefaultDate(){
        return null;
    }

    /**
     * Checks if default date is set
     *
     * @return bool true/false
     */
    public function hasDefaultDate(){
        return (!is_null($this->getDefaultDate()))? true:false;
    }

    /**
     * Checks if date is set
     *
     * @return bool true/false
     */
    public function hasDate(){
        return (!is_null($this->date))? true:false;
    }

}