<?php  namespace Fenix440\Model\Date\Traits;
use Aedart\Validate\Date\DateValidator;
use Fenix440\Model\Date\Exceptions\InvalidDateException;

/**
 * Trait EndDateTrait
 *
 * @see EndDateAware
 *
 * @package      Fenix440\Model\Date\Traits
 * @author      Bartlomiej Szala <fenix440@gmail.com>
*/
trait EndDateTrait {


    /**
     * End date for given component
     * @var null|\DateTime
     */
    protected $endDate=null;

    /**
     * Set End date for given component
     * @param \DateTime $endDate End date for given component
     * @return void
     * @throws InvalidDateException If End date is invalid
     */
    public function setEndDate($endDate){
        if(!$this->isEndDateValid($endDate))
            throw new InvalidDateException(sprintf('End date %s is invalid',var_export($endDate,true)));
        $this->endDate=$endDate;
    }

    /**
     * Validates if End date is valid
     * @param mixed $endDate Enddate for given component
     * @return bool true/false
     */
    public function isEndDateValid($endDate){
        return DateValidator::isValid($endDate);
    }

    /**
     * Get End date
     *
     * @return \DateTime|null
     */
    public function getEndDate(){
        if(!$this->hasEndDate() && $this->hasDefaultEndDate())
            $this->setEndDate($this->getDefaultEndDate());
        return $this->endDate;
    }

    /**
     * Get default End date
     *
     * @return \DateTime|null
     */
    public function getDefaultEndDate(){
        return null;
    }

    /**
     * Checks if default End date is set
     *
     * @return bool true/false
     */
    public function hasDefaultEndDate(){
        return (!is_null($this->getDefaultEndDate()))? true:false;
    }

    /**
     * Checks if End date is set
     *
     * @return bool true/false
     */
    public function hasEndDate(){
        return (!is_null($this->endDate))? true:false;
    }

}