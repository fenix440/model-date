<?php
use Fenix440\Model\Date\Traits\DateTrait;
use Fenix440\Model\Date\Interfaces\DateAware;

/**
 * Class DateTraitTest
 *
 * @coversDefaultClass Fenix440\Model\Date\Traits\DateTrait
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class DateTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /************************************************************************
     * Data "providers"
     ***********************************************************************/

    /**
     * Get the trait mock
     *
     * @return PHPUnit_Framework_MockObject_MockObject|Fenix440\Model\Date\Interfaces\DateAware
     */
    protected function getTraitMock()
    {
        return $this->getMockForTrait('Fenix440\Model\Date\Traits\DateTrait');
    }

    /************************************************************************
     * Actual tests
     ***********************************************************************/

    /**
     * @test
     *
     * @covers  ::setDate
     * @covers  ::isDateValid
     * @covers  ::getDate
     * @covers  ::getDefaultDate
     * @covers  ::hasDefaultDate
     * @covers  ::hasDate
     */
    public function setAndGetDate()
    {
        $trait = $this->getTraitMock();
        $date=new DateTime();
        $trait->setDate($date);

        $this->assertSame($date,$trait->getDate(),'Date is invalid');
        $this->assertInstanceOf('\DateTime',$trait->getDate(),'Date is not inststance of DateTime object');
    }


    /**
     * @test
     * @covers  ::setDate
     * @covers  ::isDateValid
     * @expectedException \Fenix440\Model\Date\Exceptions\InvalidDateException
     */
    public function setInvalidDate()
    {
        $trait = $this->getTraitMock();
        $date="2015-05-12 12:00:00";

        $trait->setDate($date);
    }


    /**
     * @test
     * @covers  ::getDate
     * @covers  ::getDefaultDate
     * @covers  ::hasDefaultDate
     * @covers  ::hasDate
     *
     */
    public function testDefaultDate()
    {
        $trait = $this->getTraitMock();

        $this->assertNull($trait->getDate(),'Default date is not null!');
    }

}