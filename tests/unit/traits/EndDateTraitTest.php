<?php
use Fenix440\Model\Date\Traits\EndDateTrait;
use Fenix440\Model\Date\Interfaces\EndDateAware;

/**
 * Class EndDateTraitTest
 *
 * @coversDefaultClass Fenix440\Model\Date\Traits\EndDateTrait
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class EndDateTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /************************************************************************
     * Data "providers"
     ***********************************************************************/

    /**
     * Get the trait mock
     *
     * @return PHPUnit_Framework_MockObject_MockObject|Fenix440\Model\Date\Interfaces\EndDateAware
     */
    protected function getTraitMock()
    {
        return $this->getMockForTrait('Fenix440\Model\Date\Traits\EndDateTrait');
    }

    /************************************************************************
     * Actual tests
     ***********************************************************************/

    /**
     * @test
     *
     * @covers  ::setEndDate
     * @covers  ::isEndDateValid
     * @covers  ::getEndDate
     * @covers  ::getDefaultEndDate
     * @covers  ::hasDefaultEndDate
     * @covers  ::hasEndDate
     */
    public function setAndGetEndDate()
    {
        $trait = $this->getTraitMock();
        $date=new DateTime();
        $trait->setEndDate($date);

        $this->assertSame($date,$trait->getEndDate(),'End Date is invalid');
        $this->assertInstanceOf('\DateTime',$trait->getEndDate(),'End Date is not inststance of DateTime object');
    }


    /**
     * @test
     * @covers  ::setEndDate
     * @covers  ::isEndDateValid
     * @expectedException \Fenix440\Model\Date\Exceptions\InvalidDateException
     */
    public function setInvalidEndDate()
    {
        $trait = $this->getTraitMock();
        $date="2015-05-12 12:00:00";

        $trait->setEndDate($date);
    }


    /**
     * @test
     * @covers  ::getEndDate
     * @covers  ::getDefaultEndDate
     * @covers  ::hasDefaultEndDate
     * @covers  ::hasEndDate
     *
     */
    public function testDefaultEndDate()
    {
        $trait = $this->getTraitMock();

        $this->assertNull($trait->getEndDate(),'Default End date is not null!');
    }

}