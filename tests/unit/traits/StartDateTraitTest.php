<?php
use Fenix440\Model\Date\Traits\StartDateTrait;
use Fenix440\Model\Date\Interfaces\StartDateAware;

/**
 * Class StartDateTraitTest
 *
 * @coversDefaultClass Fenix440\Model\Date\Traits\StartDateTrait
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class StartDateTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /************************************************************************
     * Data "providers"
     ***********************************************************************/

    /**
     * Get the trait mock
     *
     * @return PHPUnit_Framework_MockObject_MockObject|Fenix440\Model\Date\Interfaces\StartDateAware
     */
    protected function getTraitMock()
    {
        return $this->getMockForTrait('Fenix440\Model\Date\Traits\StartDateTrait');
    }

    /************************************************************************
     * Actual tests
     ***********************************************************************/

    /**
     * @test
     *
     * @covers  ::setStartDate
     * @covers  ::isStartDateValid
     * @covers  ::getStartDate
     * @covers  ::getDefaultStartDate
     * @covers  ::hasDefaultStartDate
     * @covers  ::hasStartDate
     */
    public function setAndGetStartDate()
    {
        $trait = $this->getTraitMock();
        $date=new DateTime();
        $trait->setStartDate($date);

        $this->assertSame($date,$trait->getStartDate(),'Start Date is invalid');
        $this->assertInstanceOf('\DateTime',$trait->getStartDate(),'Start Date is not inststance of DateTime object');
    }


    /**
     * @test
     * @covers  ::setStartDate
     * @covers  ::isStartDateValid
     * @expectedException \Fenix440\Model\Date\Exceptions\InvalidDateException
     */
    public function setInvalidStartDate()
    {
        $trait = $this->getTraitMock();
        $date="2015-05-12 12:00:00";

        $trait->setStartDate($date);
    }


    /**
     * @test
     * @covers  ::getStartDate
     * @covers  ::getDefaultStartDate
     * @covers  ::hasDefaultStartDate
     * @covers  ::hasStartDate
     *
     */
    public function testDefaultStartDate()
    {
        $trait = $this->getTraitMock();

        $this->assertNull($trait->getStartDate(),'Default Start date is not null!');
    }

}